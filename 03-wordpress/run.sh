docker run --name my-wordpress --rm \
-v `pwd`/public:/var/www/html \
-p 8081:80 \
--link my-sql-example:mysql \
wordpress:4.9.4-apache
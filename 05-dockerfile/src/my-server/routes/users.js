var express = require('express');
var faker = require('faker');
var router = express.Router();

var users = [];
var limit = 10;

for (var i = 0; i < limit; i++) {
  users.push({
    id: i,
    firstName: faker.name.firstName(),
    lastName: faker.name.lastName(),
    email: faker.internet.email(),
    image: faker.internet.avatar(),
  });
}

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send(users);
});

router.get('/:id', function(req, res, next) {
  var id = req.params.id;
  if (id < limit && id >= 0) {
    res.send(users[req.params.id]);
  } else {
    res.status(404).end('Not found');
  }
});

module.exports = router;

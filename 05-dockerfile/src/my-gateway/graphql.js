const { makeExecutableSchema } = require('graphql-tools');
const fetch = require('node-fetch');

// The GraphQL schema in string form
const typeDefs = `
  type Query { 
    users: [User]
    user(id: ID!): User
  }
  type User {
    id: ID! 
    firstName: String
    lastName: String
    email: String
    image: String
   }
`;

const userApi = process.env.USER_API || 'http://localhost:3000/users';
console.log('userApi=', userApi);

// The resolvers
const resolvers = {
  Query: {
    users: () => {
      return fetch(userApi).then(res => res.json());
    },
    user: (_, { id }) => {
      return fetch(userApi + '/' + id).then(res => res.json());
    },
  },
};

// Put together a schema
const schema = makeExecutableSchema({
  typeDefs,
  resolvers,
});

module.exports = schema;
